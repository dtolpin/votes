package model

import (
	. "bitbucket.org/dtolpin/infergo/dist"
)

type Model struct {
	NBoxes                    int
	NLastParties, NCurParties int
	LastVotes, CurVotes       [][]float64
}

func (m *Model) Observe(x []float64) float64 {
	// x is flattened matrix NCurParties*NLastParties
	lp := Normal.Logps(0, 10, x...)

	y := make([][]float64, m.NCurParties)
	j := 0
	for i := 0; i != m.NCurParties; i++ {
		jnext := j + m.NLastParties
		y[i] = make([]float64, m.NLastParties)
		D.SoftMax(x[j:jnext], y[i])
	}

	for ibox := 0; ibox != m.NBoxes; ibox++ {
		for icur := 0; icur != m.NCurParties; icur++ {
			s := 0.
			for ilast := 0; ilast != m.NLastParties; ilast++ {
				s += m.LastVotes[ibox][ilast] * y[icur][ilast]
			}
			d := s - m.CurVotes[ibox][icur]
			lp -= d * d
		}
	}

	lp /= float64(m.NBoxes)

	return lp
}
