package model

import (
	"bitbucket.org/dtolpin/infergo/ad"
	. "bitbucket.org/dtolpin/infergo/dist/ad"
)

type Model struct {
	NBoxes, NLastParties, NCurrentParties int
	LastVotes, CurrentVotes               [][]float64
}

func (m *Model) Observe(x []float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup(x)
	}
	var y [][]float64

	y = make([][]float64, m.NCurrentParties)
	var j int

	j = 0
	for i := range y {
		var jnext int

		jnext = j + m.NLastParties
		y[i] = make([]float64, m.NLastParties)
		ad.Call(func(_ []float64) {
			D.SoftMax(x[j:jnext], y[i])
		}, 0)
	}
	var lp float64
	ad.Assignment(&lp, ad.Value(0.))
	for ibox := 0; ibox != m.NBoxes; ibox = ibox + 1 {
		for icur := 0; icur != m.NCurrentParties; icur = icur + 1 {
			var s float64
			ad.Assignment(&s, ad.Value(0.))
			for ilast := 0; ilast != m.NLastParties; ilast = ilast + 1 {
				ad.Assignment(&s, ad.Arithmetic(ad.OpAdd, &s, ad.Arithmetic(ad.OpMul, &m.LastVotes[ibox][ilast], &y[icur][ilast])))
			}
			var d float64
			ad.Assignment(&d, ad.Arithmetic(ad.OpSub, &s, &m.CurrentVotes[ibox][icur]))
			ad.Assignment(&lp, ad.Arithmetic(ad.OpSub, &lp, ad.Arithmetic(ad.OpMul, &d, &d)))
		}
	}
	ad.Assignment(&lp, ad.Arithmetic(ad.OpDiv, &lp, ad.Value(float64(m.NBoxes))))

	return ad.Return(&lp)
}
