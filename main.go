package main

import (
	"bitbucket.org/dtolpin/infergo/dist"
	"bitbucket.org/dtolpin/infergo/infer"
	. "bitbucket.org/dtolpin/votes/model/ad"
	"flag"
	"fmt"
	"log"
	"math/rand"
)

func init() {
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(),
			`Estimate voter transitions:
  %s [OPTIONS]
`, os.Args[0])
		flag.PrintDefaults()
	}
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	flag.Float64Var(&RATE, "rate", RATE, "learning rate for Adam")
	flag.IntVar(&NPLATEAU, "nplateau", NPLATEAU,
		"number of iterations on plateau")
	flag.Float64Var(&EPS, "eps", EPS,
		"optimization precision, in log odds")

	rand.Seed(time.Now().UnixNano())
}

var (
	NITER = 1000
	NPLATEAU = 10
	EPS      = 0.01
	RATE = 0.1
)

func main() {
	m := &Model {
	}

	// TODO: Read the data into the model

	// Initialize the parameter vector
	x := make([]float64, m.NLastParties*m.NCurParties)
	for i := range x {
		x[i] := 0.1*rand.NormFloat64()
	}

	// Optimize the parameters
	iter, lp0, lp = infer.Optimize(
		&infer.Adam{Rate: RATE},
		m, theta,
		NITER, NPLATEAU, EPS)

	// Output the results to CSV
	j := 0
	y := make([]float64, m.NLastParties)
	for i := 0; i != m.NCurParties; i++ {
		jnext := j + m.NLastParties
		D.SoftMax(x[j:jnext], y[i])
		// TODO: print y as CSV
	}
}
