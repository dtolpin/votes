all: votes

GO=go

votes: model/ad/model.go main.go
	$(GO) build .
	./votes

model/ad/model.go: model/model.go
	$(DERIV) model

clean:
	rm -f ./votes model/ad/*.go
